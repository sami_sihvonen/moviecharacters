package com.example.springwebapi.services.franchise;

import com.example.springwebapi.models.Franchise;
import com.example.springwebapi.models.Movie;
import com.example.springwebapi.services.CrudService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Set;


public interface FranchiseService extends CrudService<Franchise,Integer> {
    void updateFrachisesMoviesById(int franchise_id, ArrayList<Integer> movieIDs);
}
