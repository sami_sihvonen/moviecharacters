package com.example.springwebapi.controllers;

import com.example.springwebapi.mappers.CharacterMapper;
import com.example.springwebapi.mappers.MovieMapper;
import com.example.springwebapi.models.Character;
import com.example.springwebapi.models.Movie;
import com.example.springwebapi.models.dtos.movie.MovieDTO;
import com.example.springwebapi.services.movie.MovieService;
import com.example.springwebapi.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;


@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {

    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;
    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Bad request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping
    public ResponseEntity findAll() {
        Collection<Movie> movies = movieService.findAll();
        return ResponseEntity.ok(movieMapper.movieToMovieDto(movies));
    }


    @Operation(summary = "Get a movie by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Moviedoes not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {

        MovieDTO movie = movieMapper.movieToMovieDto(movieService.findById(id));
        return ResponseEntity.ok(movie);
    }

    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @Operation(summary = "add a movie")
    @PostMapping
    public ResponseEntity add(@RequestBody Movie movie) {
        Movie newMovie = movieService.add(movie);
        URI uri = URI.create("movies/" + newMovie.getId());
        return ResponseEntity.created(uri).build();
    }

    @Operation(summary = "update a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody MovieDTO movieDTO, @PathVariable int id) {
        if(movieDTO.getId() != id)
            return ResponseEntity.badRequest().build();
        movieService.update(movieMapper.movieDtoToMovie(movieDTO));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "update characters in selected movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Characters in movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @PutMapping("updateCharactersInMovie/{id}")
    public ResponseEntity updateMoviesCharactersById(@RequestBody ArrayList<Integer> characterIDs, @PathVariable int id) {
        if(movieService.findById(id) == null){
            return ResponseEntity.badRequest().build();
        }

        movieService.updateMoviesCharactersById(id, characterIDs);
        return ResponseEntity.noContent().build();
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Bad request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @Operation(summary = "get all characters in selected movie")
    @GetMapping("/moviecharacters/{id}")
    public ResponseEntity getAllCharactersInMovie(@PathVariable int id) {
        Set<Character> characters = movieService.getAllCharactersInMovie(id);
        return ResponseEntity.ok(characterMapper.characterToCharacterDto(characters));
    }

}
