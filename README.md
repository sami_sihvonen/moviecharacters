## Assignment 3:  Web API and database with Spring - Movie Characters API

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![Java](https://img.shields.io/badge/-Java-red?logo=java)](https://www.java.com)
[![Spring](https://img.shields.io/badge/-Spring-white?logo=spring)](https://spring.io/)

This is a Java Spring Boot Backend Web API application with PostgreSQL database.

This is the third assignment
of the backend part of the [Noroff](https://www.noroff.no/en/) Java Full-Stack developer course.


## Table of Contents

- [Background](#background)
- [Install](#install)
- [Maintainers](#maintainers)

## Background

The task was to create a REST Api-application with databases that can be manipulated.

The documentation was made with the swagger-ui tool and can be seen in the Open API link below:

[https://moviecharacters.herokuapp.com/swagger-ui/index.html](https://moviecharacters.herokuapp.com/swagger-ui/index.html)

The application is hosted on Heroku: https://moviecharacters.herokuapp.com

This app is deployed using following tools:
 - Spring Web
 - Spring Data JPA
 - PostgreSQL
 - Docker
 - Heroku
 - Open Api

### Business rules and requirements

Data requirements:

Character:

[https://moviecharacters.herokuapp.com/api/v1/characters](https://moviecharacters.herokuapp.com/api/v1/characters)

- Id
- Full name
- Alias
- Gender
- Picture

Movie:

[https://moviecharacters.herokuapp.com/api/v1/movies](https://moviecharacters.herokuapp.com/api/v1/movies)

- Id
- Movie title
- Genre
- Release year
- Director
- Picture
- Trailer

Franchise:

[https://moviecharacters.herokuapp.com/api/v1/franchises](https://moviecharacters.herokuapp.com/api/v1/franchises)
- Id
- Name

## Install

Clone repository `git clone`


## Maintainers

[@Laura Byman](https://gitlab.com/Laura_Byman)
[@Sami Sihvonen](https://gitlab.com/sami_sihvonen)
