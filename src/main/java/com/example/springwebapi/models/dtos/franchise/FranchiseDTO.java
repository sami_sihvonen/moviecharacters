package com.example.springwebapi.models.dtos.franchise;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class FranchiseDTO {
    private int id;
    private String name;
    private String description;
}
