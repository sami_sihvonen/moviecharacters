package com.example.springwebapi.Runner;

import com.example.springwebapi.models.Character;
import com.example.springwebapi.services.character.CharacterService;
import com.example.springwebapi.services.movie.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Set;

@Component
public class AppRunner implements ApplicationRunner {
    private final CharacterService characterService;
    private final MovieService movieService;

    public AppRunner(CharacterService characterService, MovieService movieService) {
        this.characterService =characterService;
        this.movieService = movieService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Character a =characterService.findById(2);
        a.setName("testi");

        characterService.update(a);

        characterService.deleteById(1);
        characterService.findAllCharactersByFranchise(3).stream().forEach(c -> System.out.println(c.getName()));

        Set<Character> c = characterService.findAllCharactersByFranchise(3);


        //}
    }
}

